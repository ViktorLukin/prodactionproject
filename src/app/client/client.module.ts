import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AppSharedModule } from '../shared/app-shared.module';
import { ClientRoutingModule } from './client-routing.module';
import { ClientComponent } from './client.component';

@NgModule({
  imports: [CommonModule, ClientRoutingModule, AppSharedModule],
  declarations: [ClientComponent],
  providers: [],
})
export class ClientModule {}
