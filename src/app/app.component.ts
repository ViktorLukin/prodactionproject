import { Component } from '@angular/core';
// import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  public title = 'app';

  // constructor() {
  //   const backEndUrl = environment.backEndUrl;
  //
  //   // tslint:disable-next-line
  //   console.log(backEndUrl);
  //
  // }

  // tslint:disable-next-line:no-empty
  constructor() {}

  public testMethod() {
    const a = 10;
    // tslint:disable-next-line:no-console
    console.log(a);
  }
}
