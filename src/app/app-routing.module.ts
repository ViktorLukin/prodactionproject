import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './core/components/home/home.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: 'administrator',
    loadChildren: () => import('app/administrator/administrator.module').then((m) => m.AdministratorModule),
  },
  { path: 'client/:id', loadChildren: () => import('app/client/client.module').then((m) => m.ClientModule) },
  { path: 'auth', loadChildren: () => import('app/auth/auth.module').then((m) => m.AuthModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
