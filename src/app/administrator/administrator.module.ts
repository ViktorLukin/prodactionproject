import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AppSharedModule } from '../shared/app-shared.module';
import { AdministratorRoutingModule } from './administrator-routing.module';
import { AdministratorComponent } from './administrator.component';

@NgModule({
  imports: [CommonModule, AppSharedModule, AdministratorRoutingModule],
  declarations: [AdministratorComponent],
  providers: [],
})
export class AdministratorModule {}
